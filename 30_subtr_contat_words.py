from collections import defaultdict, deque


class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        num_words = len(words)
        if num_words == 0: return []
        positions = []

        w_length = len(words[0])

        occurences = defaultdict(int)

        for w in words:
            occurences[w] += 1

        for start in range(w_length):
            meet_window = defaultdict(int)
            sequence = deque()
            overmeet = set()

            for pos in range(start, len(s) - w_length + 1, w_length):
                w = s[pos: pos + w_length]
                if w in occurences:
                    sequence.append(w)
                    meet_window[w] += 1
                    if meet_window[w] > occurences[w]:
                        overmeet.add(w)

                    if len(sequence) == num_words:
                        if len(overmeet) == 0:
                            start_sequence = pos - (num_words - 1) * w_length
                            positions.append(start_sequence)

                        first = sequence.popleft()
                        meet_window[first] -= 1
                        if meet_window[first] == occurences[first]:
                            overmeet.remove(first)

                else:
                    meet_window.clear()
                    sequence.clear()
                    overmeet.clear()

        return positions