import bisect


class Solution:
    """
    Task link
    https://leetcode.com/problems/find-k-th-smallest-pair-distance/description/
    
    Idea: sort array. Use binary search to find distance.
    For each candidate distance D loop throw all numbers and use binary search to find number of pairs with
    first current value and distance lower D.
    """

    def smallestDistancePair(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        nums.sort()
        start, end = 0, nums[-1] - nums[0]
        if end == 0: return 0

        while end - start >= 0.5:
            # d - expected distance
            # strictly lower d
            numbers_lower = 0
            # lower or equal d
            numbers_lower_eq = 0
            middle = int(round((end + start) / 2))
            for i in range(0, len(nums)):
                insert_left = bisect.bisect_left(nums, middle + nums[i]) - 1
                numbers_lower += max((insert_left - i), 0)
                insert_right = bisect.bisect_right(nums, middle + nums[i])
                numbers_lower_eq += (insert_right - i) - 1

            if numbers_lower < k <= numbers_lower_eq:
                return middle
            if numbers_lower >= k:
                end = (end + start) / 2
            else:
                start = (end + start) / 2
        return -1


s = Solution()
print(s.smallestDistancePair([1, 3, 1], 1), "expected", 0)
print(s.smallestDistancePair([1, 6, 1], 3), "expected", 5)
print(s.smallestDistancePair([38, 33, 57, 65, 13, 2, 86, 75, 4, 56], 26), "expected", 36)
