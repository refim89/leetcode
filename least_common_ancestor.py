# In tree find least common ancestor for 2 nodes

tree = {0: [1, 2], 1: [3, 4], 2: [9], 3: [5, 6], 4: [7, 8], 5: [], 6: [], 7: [], 8: [], 9: [10], 10: []}
root = 0


def _dfs(tree, current_node, n1, n2):
    has_first = current_node == n1
    has_second = current_node == n2

    children = tree[current_node]

    for node in children:
        common, first_in_child, second_in_child = _dfs(tree, node, n1, n2)
        if common != -1: return common, True, True
        has_first = has_first or first_in_child
        has_second = has_second or second_in_child

    if has_first and has_second:
        return current_node, True, True

    return -1, has_first, has_second


def anchor(n1, n2, root, tree):
    least_ancestor, _, _ = _dfs(tree, root, n1, n2)
    return least_ancestor


if __name__ == '__main__':
    least_ancestor = anchor(7, 10, 0, tree)
    print(least_ancestor)
