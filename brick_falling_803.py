from collections import deque


class Solution:
    def hitBricks(self, grid, hits):
        for i, j in hits:
            grid[i][j] -= 1

        hits = hits[::-1]

        parents = {}
        m = len(grid)
        n = len(grid[0])

        for i in range(m):
            for j in range(n):
                if grid[i][j] <= 0: continue
                parents[(i, j)] = 0

        for j in range(n):
            if grid[0][j] <= 0: continue
            if parents[(0, j)] == 0:
                self.bfs(grid, parents, (0, j))

        bricks = []
        for i, j in hits:
            grid[i][j] += 1
            if grid[i][j] == 0:
                bricks.append(0)
                continue
            parents[(i, j)] = 0
            has_connection = False
            if i == 0: has_connection = True
            for delta in (-1, 1):
                if 0 <= i + delta < m and (i + delta, j) in parents and parents[(i + delta, j)] == 1:
                    has_connection = True
                if 0 <= j + delta < n and (i, j + delta) in parents and parents[(i, j + delta)] == 1:
                    has_connection = True

            if has_connection:
                parents[(i, j)] = 1
                add_bricks = self.bfs(grid, parents, (i, j))
                bricks.append(add_bricks)
            else:
                bricks.append(0)

        return bricks[::-1]

    def bfs(self, grid, parents, start_point):
        sequence = deque()
        sequence.append(start_point)

        m = len(grid)
        n = len(grid[0])

        num_connected = 0
        visited = set()

        while len(sequence) > 0:
            i, j = sequence.popleft()
            if (i, j) in visited: continue
            visited.add((i, j))
            parents[(i, j)] = 1
            num_connected += 1
            for delta in (-1, 1):
                if 0 <= i + delta < m and (i + delta, j) in parents and parents[(i + delta, j)] == 0:
                    sequence.append((i + delta, j))
                if 0 <= j + delta < n and (i, j + delta) in parents and parents[(i, j + delta)] == 0:
                    sequence.append((i, j + delta))

        return num_connected - 1


if __name__ == '__main__':
    s = Solution()
    briks = s.hitBricks([[1,1,0,1,0],[1,1,0,1,1],[0,0,0,1,1],[0,0,0,1,0],[0,0,0,0,0],[0,0,0,0,0]], [[5,1],[1,3]])
    print(briks)
