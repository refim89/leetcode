import sys


class Solution:
    def calculateMinimumHP(self, dungeon: List[List[int]]) -> int:
        n = len(dungeon)
        m = len(dungeon[0])

        min_prices = [[sys.maxsize] * m for _ in range(n)]

        start_value = dungeon[-1][-1]
        min_prices[-1][-1] = 1 + max(0, -start_value)

        for j in range(m - 1, -1, -1):
            for i in range(n - 1, -1, -1):
                if i > 0:
                    min_to_come = max(0, -dungeon[i - 1][j]) + min_prices[i][j] - max(0, dungeon[i - 1][j])
                    min_to_come = max(1, min_to_come)
                    min_prices[i - 1][j] = min(min_prices[i - 1][j], min_to_come)

                if j > 0:
                    min_to_come = max(0, -dungeon[i][j - 1]) + min_prices[i][j] - max(0, dungeon[i][j - 1])
                    min_to_come = max(1, min_to_come)
                    min_prices[i][j - 1] = min(min_prices[i][j - 1], min_to_come)

        return min_prices[0][0]