class Solution:

    def find(self, item, parents):
        if parents[item] == item: return item
        parent = self.find(parents[item], parents)
        parents[item] = parent
        return parent

    def unite(self, p1, p2, parents):
        p1 = self.find(p1, parents)
        p2 = self.find(p2, parents)
        if p1 == p2: return
        parents[p2] = p1

    def numIslands(self, grid) -> int:
        m = len(grid)
        n = len(grid[0])

        islands = {}

        last_index = 0
        for i in range(m):
            for j in range(n):
                if grid[i][j] == '1':
                    islands[(i, j)] = last_index
                    last_index += 1

        components = [i for i in range(len(islands))]

        for (i, j), current_index in islands.items():
            delta = [(1, 0), (0, 1)]
            for d in delta:
                x, y = i + d[0], j + d[1]
                if x < 0 or x >= m: continue
                if y < 0 or y >= n: continue
                if grid[x][y] == '0': continue
                neibour = islands[(x, y)]
                self.unite(current_index, neibour, components)

        islands_indexes = set()
        for c in components:
            islands_indexes.add(self.find(c, components))

        return len(set(islands_indexes))
