from heapq import heappush, heappop


class Solution:

    """
    Task link
    https://leetcode.com/problems/longest-consecutive-sequence/description/

    Solution idea: heap sort. Complexity O(n).
    Don't forget about duplicates. It must be ignored.
    """

    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        self.heap = []
        for val in nums:
            heappush(self.heap, val)

        max_len = 1
        current_length = 1

        previous = heappop(self.heap)
        while len(self.heap) > 0:
            next = heappop(self.heap)
            if next == previous + 1:
                current_length += 1
                max_len = max(current_length, max_len)
            elif next == previous:
                pass
            else:
                current_length = 1
            previous = next

        return max_len


s = Solution()
t = s.longestConsecutive([2, 0, 1, 4, 1])
print(t)

