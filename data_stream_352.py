# Definition for an interval.
class Interval:
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e


class SummaryRanges:

    """
    Task link
    https://leetcode.com/problems/data-stream-as-disjoint-intervals/description/
    """

    def __init__(self):
        # key - interval start, value - interval end
        self.intervals = {}

    def floor_key(self, key):
        if len(self.intervals) == 0: return None
        if key in self.intervals:
            return key
        candidates = [k for k in self.intervals if k < key]
        if len(candidates) == 0: return None
        return max(candidates)

    def addNum(self, val):
        """
        :type val: int
        :rtype: void
        """
        # each new number can be:
        # - add to beginning or ending for existing interval
        # - create new interval

        floor = self.floor_key(val)
        # new value already covered by interval
        if floor is not None and floor <= val <= self.intervals[floor]:
            return

        # if interval end is val-1
        if floor is not None and self.intervals[floor] == val - 1:
            self.intervals[floor] = val
            # try merge intervals
            if val + 1 in self.intervals:
                updated_ceil = self.intervals[val + 1]
                self.intervals[floor] = updated_ceil
                del self.intervals[val + 1]
                return
            return

        if val + 1 in self.intervals:
            self.intervals[val] = self.intervals[val + 1]
            del self.intervals[val + 1]
            return

        self.intervals[val] = val

    def getIntervals(self):
        """
        :rtype: List[Interval]
        """
        answer = [Interval(s, e) for s, e in self.intervals.items()]
        return sorted(answer, key=lambda interval: interval.start)


obj = SummaryRanges()
obj.addNum(0)
obj.addNum(1)
obj.addNum(2)

print(obj.intervals)
