from functools import cmp_to_key


class Solution:

    def findClosestElements(self, arr, k, x: int) :
        def comparator(v1, v2):
            if abs(v1 - x) < abs(v2 - x): return -1
            if abs(v1 - x) > abs(v2 - x): return 1
            if v1 < v2: return -1
            return 1

        arr = sorted(arr, key=cmp_to_key(comparator))

        subarray = arr[:k]
        subarray.sort()

        return subarray


if __name__ == '__main__':
    s = Solution()
    s.findClosestElements([1,1,1,10,10,10], 1, 9)