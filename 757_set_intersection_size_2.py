import math
from collections import defaultdict
import heapq


class Solution:

    def close_intervals(self, pq, intervals, opened_intervals, maxvalue):
        elements = 0
        # close all intervals
        while len(pq) > 0:
            nearest_end, close_index = pq[0]
            if nearest_end >= maxvalue:
                break

            heapq.heappop(pq)
            if opened_intervals[close_index] < 2:
                need_elements = 2 - opened_intervals[close_index]
                for k in opened_intervals:
                    interval_begin = intervals[k][0]
                    opened_intervals[k] += min(need_elements, nearest_end - interval_begin + 1)
                elements += need_elements
            del opened_intervals[close_index]

        return elements

    def intersectionSizeTwo(self, intervals: 'List[List[int]]') -> 'int':

        pq = []
        opened_intervals = defaultdict(int)

        intervals = sorted(intervals, key=lambda x: x[0])
        elements = 0

        for open_index, (x, y) in enumerate(intervals):
            elements += self.close_intervals(pq, intervals, opened_intervals, x)
            opened_intervals[open_index] = 0
            heapq.heappush(pq, (y, open_index))

        elements += self.close_intervals(pq, intervals, opened_intervals, math.pow(10, 8)+1)

        return elements


if __name__ == '__main__':
    s = Solution()
    intervals = [[1, 3], [1, 4], [2, 5], [3, 5]]
    # intervals = [[1, 3], [3, 6]]
    set_size = s.intersectionSizeTwo(intervals)
    print(set_size)

