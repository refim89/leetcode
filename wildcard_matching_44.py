# Given an input string (s) and a pattern (p), implement wildcard pattern matching with support for '?' and '*' where:
#
# '?' Matches any single character.
# '*' Matches any sequence of characters (including the empty sequence).


class Solution:
    """
    Task link:
    https://leetcode.com/problems/wildcard-matching/description/
    """

    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        ls = len(s) + 1
        lp = len(p) + 1

        achievable = [[False] * lp for _ in range(ls)]

        achievable[-1][-1] = True

        # start from last pattern symbol
        for j in range(lp-2, -1, -1):
            if p[j] == "*":
                for i in range(ls-1, -1, -1):
                    if achievable[i][j+1]:
                        for k in range(0, i+1):
                            achievable[k][j] = True
            elif p[j] == "?":
                for i in range(ls-2, -1, -1):
                    if achievable[i+1][j+1]:
                        achievable[i][j] = True
            else:
                for i in range(ls-2, -1, -1):
                    if achievable[i+1][j+1] and p[j] == s[i]:
                        achievable[i][j] = True

        return achievable[0][0]
