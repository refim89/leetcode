from collections import defaultdict


class Solution:
    def criticalConnections(self, n: int, connections):
        self.visited = set()
        self.bridges = []
        self.IN_TIME = 0
        self.tin = [n + 1] * n
        self.fup = [n + 1] * n

        self.neighbours = defaultdict(list)

        for p1, p2 in connections:
            self.neighbours[p1].append(p2)
            self.neighbours[p2].append(p1)

        start_node = connections[0][0]
        self.tin[start_node] = 0
        self.fup[start_node] = self.IN_TIME

        self.dfs(start_node, parent=-1)

        return self.bridges

    def dfs(self, input_node, parent):
        self.tin[input_node] = self.IN_TIME
        self.IN_TIME += 1
        self.visited.add(input_node)
        self.fup[input_node] = min(self.fup[input_node], self.tin[input_node])

        for neighbour in self.neighbours[input_node]:
            # skip parent
            if neighbour == parent: continue

            if neighbour in self.visited:
                # one of children nodes has edge to parent
                self.fup[input_node] = min(self.fup[input_node], self.fup[neighbour])
                continue

            self.dfs(neighbour, input_node)
            # one of children nodes has edge to parent
            self.fup[input_node] = min(self.fup[input_node], self.fup[neighbour])

            if self.fup[neighbour] > self.tin[input_node]:
                # child node could be visited only after visiting current node
                self.bridges.append([input_node, neighbour])


if __name__ == '__main__':
    s = Solution()
    # bridges = s.criticalConnections(3, [[0,1],[1,2],[2,0]])
    bridges = s.criticalConnections(4, [[0,1],[1,2],[2,0],[1,3]])
    # bridges = s.criticalConnections(2, [[0,1]])
    print(bridges)