from heapq import heappush, heappop


class MedianFinder:
    """
    Task link
    https://leetcode.com/problems/find-median-from-data-stream/description/

    Solution idea: store 2 heaps with equal len. First store smaller part of received values and
    second store bigger part of values.
    """

    def __init__(self):
        # min_heap contains k or k+1 elements while max_heap k
        self.min_heap = []
        self.max_heap = []

    def addNum(self, num):
        """
        :type num: int
        :rtype: void
        """
        if len(self.min_heap) == len(self.max_heap):
            heappush(self.min_heap, -num)
        else:
            heappush(self.max_heap, num)

        if len(self.max_heap) == 0: return

        # swap heap elements
        from_min = -heappop(self.min_heap)
        from_max = heappop(self.max_heap)

        if from_max > from_min:
            # put them back
            heappush(self.min_heap, -from_min)
            heappush(self.max_heap, from_max)
        else:
            # swap
            heappush(self.min_heap, -from_max)
            heappush(self.max_heap, from_min)

    def findMedian(self):
        """
        :rtype: float
        """
        if len(self.min_heap) != len(self.max_heap):
            return -self.min_heap[0]
        else:
            return (0.0 + -self.min_heap[0] + self.max_heap[0]) / 2


# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()

s = MedianFinder()
s.addNum(1)
s.addNum(2)
print(s.findMedian())
