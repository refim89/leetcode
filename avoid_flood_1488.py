from collections import deque


class Solution:
    def avoidFlood(self, rains):
        n = len(rains)
        output = [-1] * n

        last_meet = {}
        dry_indexes = deque()
        for ind, lake in enumerate(rains):
            if lake == 0:
                dry_indexes.append(ind)
                continue

            if lake in last_meet:
                prev_ind = last_meet[lake]
                min_dry_ind = self.bin_search(dry_indexes, prev_ind)
                if min_dry_ind == -1: return []
                for i in range(min_dry_ind, len(dry_indexes)):
                    indd = dry_indexes[i]
                    if output[indd] == -1:
                        output[indd] = lake
                        if i == 0:
                            dry_indexes.popleft()
                        elif i == len(dry_indexes) - 1:
                            dry_indexes.pop()
                        break
                    if i == len(dry_indexes) - 1: return []

            last_meet[lake] = ind

        for ind in dry_indexes:
            if output[ind] == -1: output[ind] = 1

        return output

    def bin_search(self, arr, v):
        if len(arr) == 0: return -1
        start = 0
        end = len(arr) - 1

        while end - start > 1:
            middle = (end + start) // 2
            if arr[middle] > v:
                end = middle
            else:
                start = middle

        if arr[start] > v: return start
        if arr[end] > v: return end
        return -1


if __name__ == '__main__':
    s = Solution()
    s.avoidFlood(rains=[2,3,0,0,3,1,0,1,0,2,2])