import math

# https://leetcode.com/problems/consecutive-numbers-sum/
# sum of consuqutive elements
# (p + (p + k))(k+1) / 2 =  N => k*k < 2*N  => k< sqrt(2*N)
# find number of sulutions in int numbers

class Solution:
    def consecutiveNumbersSum(self, N):
        """
        :type N: int
        :rtype: int
        """
        max_k = int(math.sqrt(2 * N)) + 1
        c = 1
        for k in range(1, max_k + 1):
            if (2 * N - k * k - k) % (2 * (k + 1)) == 0 and (2 * N - k * k - k) > 0:
                c += 1
        return c
