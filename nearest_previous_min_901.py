from collections import deque


class StockSpanner:

    def __init__(self):
        self.d = deque()

    def next(self, price: int) -> int:
        days = 1
        while len(self.d) > 0:
            v, seq = self.d[-1]
            if v <= price:
                days += seq
                self.d.pop()
            else: break

        self.d.append((price, days))

        return days


# Your StockSpanner object will be instantiated and called as such:
# obj = StockSpanner()
# param_1 = obj.next(price)
