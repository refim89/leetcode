# Definition for a point.
# class Point:
#     def __init__(self, a=0, b=0):
#         self.x = a
#         self.y = b

from collections import defaultdict

import math


class Solution:

    """
    Task link:
    https://leetcode.com/problems/max-points-on-a-line/description/
    """

    def gcd(self, a, b):
        if a == 0 or b == 0: return max(a, b)
        if a > b:
            return self.gcd(a % b, b)
        else:
            return self.gcd(a, b % a)

    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """

        if len(points) <= 2: return len(points)
        max_points = 2

        # iterate over all points and calculate duplicate slop for each point pair
        for index, p in enumerate(points[:-1]):

            # p = (x0, y0) is fixed
            # y = k*x + b
            # line (p1, p2) has same slope as line (p1, p3) then p1 p2 p3 on same line
            # so we need only calculate slope
            # y1 = k*x1 + b and y2 = k*x2 +b - same line, then
            # k = (y2-y1)/(x2-x1)

            slope_dict = defaultdict(int)
            duplicates = 0

            local_max = 0

            for candidate_point in points[index + 1:]:
                if candidate_point.x == p.x and candidate_point.y == p.y:
                    duplicates += 1
                    continue

                # due to double precision use pair (y2-y1), (x2-x1) as k, but divide by gcd
                k = (candidate_point.y - p.y), (candidate_point.x - p.x)

                # if point lie not vertical or horizontal line
                if k[0] != 0 and k[1] != 0:
                    gcd = self.gcd(math.fabs(k[0]), math.fabs(k[1]))
                    k = tuple([int(x / gcd) for x in k])
                else:
                    if k[0] == 0: k = (0, 1)
                    if k[1] == 0: k = (1, 0)

                # (-1, 1) is the same as (1, -1) because -1/1 = 1/-1
                # the same with (-1 -1) = (1, 1)
                # lets make first arg always positive
                if k[0] < 0: k = (-k[0], -k[1])

                slope_dict[k] += 1
                local_max = max(local_max, slope_dict[k])

            # count off base point
            local_max += 1 + duplicates
            max_points = max(max_points, local_max)

        return max_points
