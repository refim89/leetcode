class Solution:
    hightest = 0

    def is_reachable(self, part_roads, height):
        if sum(part_roads) < height: return False
        max_sum = height
        possible_sums = [False] * (max_sum+1)
        possible_sums[0] = True
        for r in part_roads:
            for i in range(max_sum, r-1, -1):
                if possible_sums[i-r]:
                    possible_sums[i] = True
        return possible_sums[height]

    def search(self, roads, taken_indexes:list, index):
        if index == len(roads):
            height = sum([roads[i] for i in taken_indexes])
            if height <= self.hightest: return
            second_billboard = [roads[i] for i in range(len(roads)) if i not in taken_indexes]
            if self.is_reachable(second_billboard, height):
                self.hightest = height
        else:
            self.search(roads, taken_indexes, index+1)
            taken_indexes.append(index)
            self.search(roads, taken_indexes, index+1)
            taken_indexes.remove(taken_indexes[-1])

    def tallestBillboard(self, rods):
        """
        :type rods: List[int]
        :rtype: int
        """
        self.search(rods, [], 0)
        return self.hightest


if __name__ == '__main__':
    s = Solution()
    c = s.tallestBillboard([1, 2, 3, 6])
    print(c)