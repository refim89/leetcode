class Solution:

    def jobScheduling(self, startTime, endTime, profit) -> int:
        jobs = list(zip(startTime, endTime, profit))
        jobs.sort(key=lambda x: x[1])

        max_profit = [[0, 0]]

        for start, end, job_profit in jobs:
            previous_max = self.bin_search(max_profit, start, 0, len(max_profit) - 1)

            last_end, last_max = max_profit[-1]
            current_max = max(job_profit + previous_max, last_max)

            max_profit.append((end, current_max))

        return max_profit[-1][1]

    def bin_search(self, max_profit, value, begin, end):
        if len(max_profit) == 0: return 0
        if max_profit[begin][0] > value: return 0
        if begin == end: return max_profit[begin][1]
        if end == begin + 1:
            if max_profit[end][0] > value:
                return max_profit[begin][1]
            else:
                return max_profit[end][1]

        middle = (begin + end) // 2
        if max_profit[middle][0] > value:
            return self.bin_search(max_profit, value, begin, middle - 1)
        else:
            return self.bin_search(max_profit, value, middle, end)


def bin_search(array, value, begin, end):
    """
    Find smallest ind in array with array[ind] > value.
    If all items less => end + 1.
    """
    if len(array) == 0: return -1
    if array[begin] > value: return begin
    if end - begin <= 1:
        return end if array[end] > value else end + 1

    middle = (begin + end) // 2
    if array[middle] <= value:
        return bin_search(array, value, middle, end)
    else:
        return bin_search(array, value, begin, middle-1)


def expected(array, v):
    for i in range(len(array)):
        if array[i] > v: return i

    return len(array)


if __name__ == '__main__':
    x = [-2, 1, 1, 2, 2, 2, 3, 5, 5, 8, 8, 8]
    for i in range(-5, 10):
        exp = expected(x, i)
        print(i, exp, bin_search(x, i, 0, len(x) - 1))
