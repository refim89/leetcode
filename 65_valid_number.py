class Solution:

    def _valid(self, s, point_allowed):
        digits = set([str(i) for i in range(10)])

        meet_point = False
        meet_digits = False
        for i, v in enumerate(s):
            if v in ('+', '-'):
                if i > 0: return False
            elif v == '.':
                if not point_allowed: return False
                # is ok '.752' ?
                # is ok '1.' ?

                if meet_point: return False
                meet_point = True
            elif v in digits:
                meet_digits = True
            else: return False

        return meet_digits


    def isNumber(self, s: str) -> bool:
        s = s.strip()

        digits = set([str(i) for i in range(10)])
        allowed_symbols = digits | {'.', 'e', '+', '-'}

        e_index = -1
        for index, symbol in enumerate(s):
            if symbol not in allowed_symbols: return False
            if symbol == 'e':
                if e_index == -1: e_index = index
                else: return False

        if e_index != -1:
            before_e = self._valid(s[:e_index], point_allowed=True)
            after_e = self._valid(s[e_index+1:], point_allowed=False)
            return before_e and after_e

        return self._valid(s, point_allowed=True)


