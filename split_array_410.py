class Solution:
    """
    Task link
    https://leetcode.com/problems/split-array-largest-sum/description/
    """

    # O(ln(sum(nums)) * len(nums))
    # binary search to find largest sum + greedy approach to split array
    # this solution ok for non-negative arrays
    def splitArray(self, nums, n_splits):
        """
        :type nums: List[int]
        :type splits: int
        :rtype: int
        """
        total_sum = sum(nums)

        smallest = 0
        largest = total_sum
        middle = (smallest + largest) / 2
        achievable_sum = total_sum

        while largest - smallest > 0.5:
            achievable, max_sum = self.__is_achievable(nums, n_splits, middle)
            if achievable:
                achievable_sum = max_sum
                largest = middle
            else:
                smallest = middle
            middle = (smallest + largest) / 2

        return achievable_sum

    def __is_achievable(self, nums, n_splits, max_threshold):
        """
        :param nums:
        :param n_splits:
        :param max_threshold:
        :return: (achievable, max_sum)
            achievable - if array could be split in n_splits parts with sum less than max_threshold
            max_sum - max part sum if achievable
        """
        max_sum = -1

        part_sum = 0
        parts = 0
        for element in nums:
            if element > max_threshold: return False, -1
            if part_sum + element > max_threshold:
                max_sum = max(max_sum, part_sum)
                part_sum = 0
                parts += 1
            part_sum += element

        parts += 1
        max_sum = max(max_sum, part_sum)

        return n_splits >= parts, max_sum


    # O(n_splits * len(nums)**2)
    # dp by array position and number of splits
    def splitArrayDP(self, nums, n_splits):
        """
        :type nums: List[int]
        :type splits: int
        :rtype: int
        """
        points = len(nums)

        sums = [0] * points
        sums[0] = nums[0]

        for i in range(1, points):
            sums[i] = sums[i - 1] + nums[i]

        # dp[i][j] - largest sum
        # if we processed interval [0-i] and made j splits
        # the last split is after i position
        dp = [[0] * (n_splits + 1) for _ in range(points)]

        # init - if first split was made in position i
        for i in range(points):
            dp[i][1] = sums[i]

        for split in range(2, n_splits + 1):
            for i in range(split - 1, points):
                m = sums[-1]
                for j in range(1, i + 1):
                    t = max(dp[i - j][split - 1], sums[i] - sums[i - j])
                    m = min(t, m)
                dp[i][split] = m

        return dp[points - 1][n_splits]


s = Solution()
t = s.splitArray([7,2,5,10,8], 2)
print(t)