from collections import defaultdict, OrderedDict


class Solution:

    def _find(self, parents, index):
        if parents[index] == index: return index
        parent = self._find(parents, parents[index])
        parents[index] = parent
        return parent

    def _union(self, parents, a, b):
        i1 = self._find(parents, a)
        i2 = self._find(parents, b)
        min_index = min(i1, i2)
        max_index = max(i1, i2)
        parents[a] = min_index
        parents[max_index] = min_index

    def _connect(self, positions, m, n):
        parents = [i for i in range(m + n)]
        for r, c in positions:
            self._union(parents, r, m + c)

        group_positions = defaultdict(list)
        for r, c in positions:
            group = self._find(parents, r)
            group_positions[group].append((r, c))

        for family in group_positions.values():
            yield family

    def matrixRankTransform(self, matrix):
        m, n = len(matrix), len(matrix[0])

        answer = [[0 for _ in range(n)] for __ in range(m)]
        item_positions = defaultdict(list)

        for i in range(m):
            for j in range(n):
                item_positions[matrix[i][j]].append((i, j))

        item_positions = OrderedDict(sorted(item_positions.items(), key=lambda t: t[0]))

        max_row_value = [0 for _ in range(m)]
        max_col_value = [0 for _ in range(n)]

        for item, positions in item_positions.items():
            for family in self._connect(positions, m, n):
                current_max = 0
                for i, j in family:
                    current_max = max(current_max, max_row_value[i])
                    current_max = max(current_max, max_col_value[j])

                for i, j in family:
                    answer[i][j] = current_max + 1
                    max_row_value[i] = current_max + 1
                    max_col_value[j] = current_max + 1

        return answer
