# https://leetcode.com/problems/palindrome-pairs/
# Idea:
# split each word in 2 parts: prefix + suffix
# if reversed prefix exist in word list => check if suffix palindrome
#
# also possible situation when reversed word consist of palindrome + suffix, so reverse all words, solve same task
# for reversed and concatenate results

class Solution:

    def isPalindrome(self, suffix):
        i = 0
        j = len(suffix) - 1
        while j > i:
            if suffix[i] != suffix[j]: return False
            i += 1
            j -= 1
        return True

    def _pairs(self, words):
        reversed = {}
        for i, w in enumerate(words):
            w = w[::-1]
            reversed[w] = i

        palindromes = set()

        for w_index, w in enumerate(words):
            for i in range(0, len(w)+1):
                prefix = w[:i]
                if not prefix in reversed: continue
                if w_index == reversed[prefix]: continue
                if self.isPalindrome(w[i:]):
                    palindromes.add((w_index, reversed[prefix]))

        return palindromes



    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """

        first_word_palindrome = self._pairs(words)
        reversed = [w[::-1] for w in words]
        second_word_palindrome = self._pairs(reversed)
        second_word_palindrome = {(j, i) for (i, j) in second_word_palindrome}

        palindromes = first_word_palindrome | second_word_palindrome
        palindromes = list(palindromes)
        return palindromes



if __name__ == '__main__':
    s = Solution()
    # t = s.palindromePairs(["abcd", "dcba", "lls", "s", "sssll"])
    # t = s.palindromePairs(["lls", "s" ])
    # t = s.palindromePairs(["lls", "s", "sssll"])
    t = s.palindromePairs(["a", ""])
    # t = s.palindromePairs(["a", "b", "c", "ab", "ac", "aa"])
    print(t)
