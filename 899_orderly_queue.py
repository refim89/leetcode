# https://leetcode.com/problems/orderly-queue/
# Idea:
# if K == 1 find best rotate
# if K>=2 we can swap any 2 consecutive letters:
# lets S = aacb
# aacb => acba => cbaa => caab => aabc
# So we can make ordered string

class Solution:
    def orderlyQueue(self, S: 'str', K: 'int') -> 'str':
        if K>=2: return "".join(sorted(S))

        best_shift = S
        for i in range(0, len(S)):
            rotated = S[i:] + S[:i]
            if rotated < best_shift:
                best_shift = rotated

        return best_shift

