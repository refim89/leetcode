class Solution:
    """
    Task link
    https://leetcode.com/problems/candy/description/
    """

    def candy(self, ratings):
        """
        :type ratings: List[int]
        :rtype: int
        """

        candies = [-1] * len(ratings)

        positions = list((r, i) for i, r in enumerate(ratings))
        positions = sorted(positions, key=lambda x: x[0])

        # sort positions by rating
        # find growing sequences
        # for sequence min give 1, for growing neighbour give max(k+1, current_value)  while it grows

        for r, p in positions:
            if candies[p] == -1:
                receive = 1
                index = p
                candies[p] = receive

                # move left
                while index > 0 and candies[index - 1] < receive + 1:
                    if ratings[index - 1] > ratings[index]:
                        receive += 1
                        candies[index - 1] = receive
                        index -= 1
                    else:
                        break

                # move right
                receive = 1
                index = p

                while index < len(ratings) - 1 and candies[index + 1] < receive + 1:
                    if ratings[index + 1] > ratings[index]:
                        receive += 1
                        candies[index + 1] = receive
                        index += 1
                    else:
                        break

        return sum(candies)


s = Solution()
# s.candy([4, 2, 1, 3, 5])
s.candy([4, 2, 3, 4, 1])
