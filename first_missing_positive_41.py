class Solution:
    """
    Task link
    https://leetcode.com/problems/first-missing-positive/description/
    """

    def firstMissingPositive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)

        def do_swaps(current_position):
            if nums[current_position] == current_position + 1 or \
                            nums[current_position] < 1 or \
                            nums[current_position] > n:
                return

            # swap value in current_position with value in nums[current_position]
            current_value = nums[current_position]
            # avoid infinity swap in such situation [1, 1]
            if nums[current_value - 1] == current_value: return

            swap_with = nums[current_value - 1]
            nums[current_value - 1] = current_value
            nums[current_position] = swap_with
            do_swaps(current_position)

        for i in range(n):
            do_swaps(i)

        for i in range(n):
            if nums[i] != i + 1: return i + 1

        return n + 1


s = Solution()
print(s.firstMissingPositive([3, 4, -1, 1, 1]))
