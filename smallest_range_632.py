from heapq import heappush, heappop


class Solution(object):
    """
    Task link
    https://leetcode.com/problems/smallest-range/description/

    Solution idea: save in heap one element from each array. That is possible range begins.
    Save heap max - that is range end. After pop element put next array element from array.
    """
    def smallestRange(self, nums):
        """
        :type nums: List[List[int]]
        :rtype: List[int]
        """

        # contains tuples: (array_value, array_index, position in array)
        range_begins = []

        heap_max = -1e5
        k = len(nums)
        min_range = (-1e5, 1e5)

        for array_index, array_num in enumerate(nums):
            heappush(range_begins, (array_num[0], array_index, 0))
            heap_max = max(array_num[0], heap_max)

        while len(range_begins) == k:
            start, head_array, head_position = heappop(range_begins)

            max_end = heap_max

            if max_end - start < min_range[1]-min_range[0]:
                min_range = (start, max_end)

            if head_position < len(nums[head_array])-1:
                array = nums[head_array]
                heappush(range_begins, (array[head_position+1], head_array, head_position+1))
                heap_max = max(array[head_position+1], heap_max)

        return min_range


s = Solution()
t = s.smallestRange([[4,10,15,24,26],[0,9,12,20],[5,18,22,30]])
print(t)