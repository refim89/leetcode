
# https://leetcode.com/problems/non-negative-integers-without-consecutive-ones/
# Idea: lets f(k) is answer for (2^k - 1), then f(k+1) = f(k) + f(k-1)
# because we take all cases if number starts with 0 and k digits + all cases when number starts with 10 and k-1 digits
# if n != (2^k - 1) we find max k such n > (2^k - 1) and
# if n > 110..0 (k zeros) answer is f(k) + f(k-1)
# otherwise f(k) +  answer for n - 2^k


class Solution:
    def pow2stats(self, n):
        dp = [1, 2]
        i = 2
        while (n >> i) > 0:
            dp.append(dp[i - 1] + dp[i - 2])
            i += 1
        return dp

    def conseq(self, n, dp):
        if n == 0: return 1
        if n == 1: return 2

        max_pow = 0
        while (n >> (max_pow + 1)) - 1 >= 0:
            max_pow += 1

        threshold = (1 << max_pow) + (1 << (max_pow - 1))
        if n >= threshold:
            if max_pow >= 1:
                return dp[max_pow] + dp[max_pow-1]
            else:
                return dp[max_pow]
        else:
            return dp[max_pow] + self.conseq(n - (1 << max_pow), dp)

    def findIntegers(self, num: 'int') -> 'int':
        if num == 0: return 1
        if num == 1: return 2
        if num == 2: return 3

        dp = self.pow2stats(num)
        return self.conseq(num, dp)

