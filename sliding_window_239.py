from collections import defaultdict
from heapq import heappush, heappop


# https://leetcode.com/problems/sliding-window-maximum/
# use heap to get maxes and use dict with count elements frequency from array beginning
# remove element from heap top if it present in array beginning

class Solution:
    def maxSlidingWindow(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        h = []
        maxes = []
        should_be_removed = defaultdict(int)

        def clear_heap():
            top = -h[0]
            while should_be_removed[top] > 0:
                heappop(h)
                should_be_removed[top] -= 1
                top = -h[0]

        for index, value in enumerate(nums):
            heappush(h, -value)
            if index == k - 1: maxes.append(-h[0])
            if index >= k:
                old_value = nums[index - k]
                should_be_removed[old_value] += 1
                clear_heap()

                top = -h[0]
                maxes.append(top)

        return maxes


if __name__ == '__main__':
    s = Solution()
    maxes = s.maxSlidingWindow([1, 3, -1, -3, 5, 3, 6, 7], k=3)
    print(maxes)
