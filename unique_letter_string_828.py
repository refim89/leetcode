from collections import defaultdict

# https://leetcode.com/problems/unique-letter-string/
# For each letter find all its locations. Letter will be unique in all substring which begin after previous letter
# occurrence and end before next letter occurrence.


class Solution:
    def uniqueLetterString(self, S: 'str') -> 'int':

        occurences = defaultdict(list)
        for index, letter in enumerate(S):
            occurences[letter].append(index)

        mod = int(1e9 + 7)

        unique_substrings = 0
        for _, indexes in occurences.items():
            indexes.append(len(S))
            previous_index = -1
            for i in range(len(indexes) - 1):
                current_index = indexes[i]
                letters_before = current_index - previous_index
                letters_after = indexes[i+1] - current_index
                unique_substrings += letters_before * letters_after
                unique_substrings %= mod
                previous_index = current_index

        return unique_substrings
