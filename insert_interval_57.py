import bisect


# Definition for an interval.
class Interval:
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

    def __repr__(self):
        return "(" + str(self.start) + " " + str(self.end) + ")"


class Solution:

    """
    Task link
    https://leetcode.com/problems/insert-interval/description/
    """

    def insert(self, intervals, newInterval):
        """
        :type intervals: List[Interval]
        :type newInterval: Interval
        :rtype: List[Interval]
        """

        if len(intervals) == 0:
            return [newInterval]

        starts = [x.start for x in intervals]

        insert_position = bisect.bisect_left(starts, newInterval.start)
        max_covered_position = bisect.bisect_right(starts, newInterval.end)

        new_start, new_end = newInterval.start, newInterval.end

        # find extended interval
        if insert_position > 0:
            # check intersection with left interval
            if intervals[insert_position-1].end >= new_start:
                new_start = min(new_start, intervals[insert_position-1].start)

        if max_covered_position > 0:
            if intervals[max_covered_position-1].start <= new_end:
                new_end = max(new_end, intervals[max_covered_position-1].end)

        insert_position = bisect.bisect_left(starts, new_start)
        max_covered_position = bisect.bisect_right(starts, new_end)

        intervals = intervals[:insert_position] + [Interval(new_start, new_end)] + intervals[max_covered_position:]

        # print(intervals)

        return intervals


intervals = [Interval(1, 5)]
s = Solution()
s.insert(intervals, Interval(5, 8))
