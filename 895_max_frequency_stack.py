from collections import defaultdict
from heapq import heappush, heappop

# https://leetcode.com/problems/maximum-frequency-stack/
# Idea: save in priority queue frequency and last insert index

class FreqStack:

    def __init__(self):
        self.frequencies = defaultdict(int)
        self.priority_queue = []
        self.last_index = 0

    def push(self, x: int) -> None:
        self.frequencies[x] += 1
        heappush(self.priority_queue, (-self.frequencies[x], -self.last_index, x))
        self.last_index += 1

    def pop(self) -> int:
        _, _, x = heappop(self.priority_queue)
        self.frequencies[x] -= 1
        return x


# Your FreqStack object will be instantiated and called as such:
# obj = FreqStack()
# obj.push(x)
# param_2 = obj.pop()
