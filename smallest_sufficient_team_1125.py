from collections import defaultdict


class Solution:
    def smallestSufficientTeam(self, req_skills: List[str], people: List[List[str]]) -> List[int]:
        skill_id = {}
        target_skill = 0
        for ind, skill in enumerate(req_skills):
            skill_id[skill] = ind
            target_skill += (1 << ind)

        person_skills = {}
        group_skills = defaultdict(set)

        for ind, pskill in enumerate(people):
            ubm = 0
            for skill in pskill:
                ubm += (1 << skill_id[skill])
            person_skills[ind] = ubm

            next_group_skills = defaultdict(set)
            next_group_skills[ubm] = {ind}

            for bm, users in group_skills.items():
                self.add_bm(next_group_skills, bm, users)
                self.add_bm(next_group_skills, bm | ubm, users | {ind})

            group_skills = next_group_skills

        return group_skills[target_skill]

    def add_bm(self, next_group_skills, bm, users):
        if bm not in next_group_skills:
            next_group_skills[bm] = users
        else:
            users_next = len(next_group_skills[bm])
            if len(users) < users_next:
                next_group_skills[bm] = users
