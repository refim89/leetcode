class Solution:
    def findLatestStep(self, arr, m: int) -> int:
        n = len(arr)
        used = [False] * (n + 2)
        self.group_size = {}
        self.group_id = list(range(n + 2))

        group_step = -1
        step = 1

        for i in arr:
            used[i] = True
            self.group_size[i] = 1
            if used[i - 1]: self.unite(i, i - 1)
            if used[i + 1]: self.unite(i, i + 1)

            if m in self.group_size.values():
                group_step = step

            step += 1

        return group_step

    def unite(self, g1, g2):
        p1 = self.find(g1)
        p2 = self.find(g2)
        if p1 == p2: return

        s1 = self.group_size[p1]
        s2 = self.group_size[p2]
        self.group_size.pop(p1)

        self.group_id[p1] = p2
        self.group_size[p2] = s1 + s2

    def find(self, g):
        if self.group_id[g] == g: return g
        parent = self.find(self.group_id[g])
        self.group_id[g] = parent
        return parent


if __name__ == '__main__':
    s = Solution()
    s.findLatestStep([3,5,1,2,4], 1)