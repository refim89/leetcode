# https://leetcode.com/problems/valid-permutations-for-di-sequence/
# Idea: we don't care which exactly numbers were before, we only care about last number and RANG of number we put
# now. Example: (?, ?, 2) and left {1, 5} is the same situation if we have (?, ?, 1) and left {0, 2}.
# Lets we try put number with RANG R, and left rangs {0, .., R-1, R+1, .. k} and we processed (n-k-1) letters
# If 'I' we can put it after all sequences with last rang 0..R
# if 'D' - after last rang R+1..k


class Solution:

    def numPermsDISequence(self, S: 'str') -> 'int':
        MOD = 10**9 + 7
        nums = len(S) + 1
        # dp[i][j] - num sequences with length i and j is last rank index in remaining values
        dp = [[0] * nums for _ in range(nums)]

        # we have 1 seq with length 1
        for j in range(nums):
            dp[0][j] = 1

        for i in range(1, nums):
            for j in range(nums - i):
                if S[i - 1] == 'I':
                    for k in range(0, j + 1):
                        dp[i][j] = (dp[i][j] + dp[i - 1][k]) % MOD
                else:
                    for k in range(j + 1, nums - i + 1):
                        dp[i][j] = (dp[i][j] + dp[i - 1][k]) % MOD

        # import numpy as np
        # print(np.array(dp))
        return dp[nums-1][0]


if __name__ == '__main__':
    s = Solution()
    s.numPermsDISequence("DD")
    print()

    s.numPermsDISequence("II")
    print()

    s.numPermsDISequence("ID")
    print()

    s.numPermsDISequence("DID")
    print()

