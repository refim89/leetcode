class Solution:

    """
    Task link
    https://leetcode.com/problems/remove-duplicate-letters/description/#

    Idea. Find first letter: first appearance of lowest letter, after each all other letters present.
    Next letter - find in the same way ignoring first.
    Complexity- O(k*n)
    k - number of distinct letters, n - string length
    """

    @staticmethod
    def find_smallest_letter(s, position, used, distinct):
        left_letters = list(distinct - used)
        left_letters.sort()

        for c in left_letters:
            c_position = s.index(c, position)
            letters_after = set(s[c_position:]) - used
            if letters_after == set(left_letters):
                return c, c_position

        return "", -1

    def removeDuplicateLetters(self, s):
        """
        :type s: str
        :rtype: str
        """
        distinct = {c for c in s}
        used = set()

        smallest_str = ""
        last_position = -1
        while len(smallest_str) < len(distinct):
            letter, position = Solution.find_smallest_letter(s, last_position + 1, used, distinct)
            smallest_str += letter
            used.add(letter)
            last_position = position

        return smallest_str

t = "bcabc"
s = Solution()
print(s.removeDuplicateLetters(t))