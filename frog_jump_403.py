class Solution:

    """
    Task link
    https://leetcode.com/problems/frog-jump/description/
    """

    def canCross(self, stones):
        """
        :type stones: List[int]
        :rtype: bool
        """

        last_stone = stones[-1]
        set_stones = set(stones)

        # contains (position, k) from which goal is not reachable
        failing_location = set()

        # recursion + memorizing bad positions
        def can_finish(stone, k):
            if (stone, k) in failing_location: return False
            if stone == last_stone: return True

            if k > 2 and (stone + k - 1) in set_stones:
                if can_finish(stone + k - 1, k - 1): return True

            if k > 0 and (stone + k) in set_stones:
                if can_finish(stone + k, k): return True

            if (stone + k + 1) in set_stones:
                if can_finish(stone + k + 1, k + 1): return True

            failing_location.add((stone, k))
            return False

        return can_finish(stones[0], 0)


s = Solution()
t = s.canCross([0, 1, 3, 4, 5, 7, 9, 10, 12])
print(t)
