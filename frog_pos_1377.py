from collections import defaultdict


class Solution:
    def frogPosition(self, n: int, edges, t: int, target: int) -> float:
        graph = defaultdict(list)

        for v1, v2 in edges:
            graph[v1].append(v2)
            graph[v2].append(v1)

        start_node = 1
        v_probs = [(start_node, 1.0)]
        if len(graph[start_node]) == 0: return 1.0
        if target == start_node: return 0.0

        visited = set()

        for tick in range(t + 1):
            next_v_probs = []

            for v, prob in v_probs:
                visited.add(v)
                if v == target:
                    return prob if (len(graph[v]) == 1 or (tick == t)) else 0.0

                for neig in graph[v]:
                    if neig in visited: continue
                    near = len(graph[v])
                    if v != start_node: near -= 1
                    next_v_probs.append((neig, prob / near))

            v_probs = next_v_probs

        return 0.0


if __name__ == '__main__':
    s = Solution()
    p = s.frogPosition(7, [[1,2],[1,3],[1,7],[2,4],[2,6],[3,5]], 2, 4)
    print(p)
