# Given a string s, partition s such that every substring of the partition is a palindrome.


class Solution:

    def minCut(self, s: str) -> int:

        n = len(s)
        # min cuts for substring 0-i
        cuts = [n + 1] * n
        cuts[0] = 0

        palindromes = [[False] * n for _ in range(n)]

        # palindromes matrix init by substring size
        for substr_size in range(n):
            for start in range(n - substr_size):
                end = start + substr_size
                if start == end:
                    palindromes[start][end] = True
                elif end == start + 1:
                    palindromes[start][end] = s[start] == s[end]
                else:
                    palindromes[start][end] = s[start] == s[end] and palindromes[start + 1][end - 1]

        for i in range(1, n):
            cuts[i] = cuts[i - 1] + 1
            for j in range(i + 1):
                if palindromes[j][i]:
                    if j == 0:
                        cuts[i] = 0
                    else:
                        cuts[i] = min(cuts[i], cuts[j - 1] + 1)

        return cuts[-1]
