"""
class Solution {

    // Task link https://leetcode.com/problems/max-sum-of-rectangle-no-larger-than-k/description/
    // Idea:
    // try each pair of columns as rectangle boundary
    // calculate comulative sum for rows from 0 in this bounded rectangle and
    // use TreeSet to find rectangle with rows (0, i) with ceiling sum (comSum-k)
    // then closets sum will be in rect with rows (i+1, j) and sum
    // comSum - ceiling(comSum-k)

    public int maxSumSubmatrix(int[][] matrix, int k) {

        int integralSums[][] = new int[matrix.length][matrix[0].length+1];
        // add imaginary first column
        for (int i=0;i<matrix.length;i++){
            for (int j=1;j<matrix[0].length+1;j++){
                integralSums[i][j] = integralSums[i][j-1] + matrix[i][j-1];
            }
        }

        // try all column pairs as first and last rectangle bound
        // first column is not used, last used
        int closest = -Integer.MAX_VALUE;
        for (int i=0;i<matrix[0].length;i++){
            for (int j=i+1;j<matrix[0].length+1;j++){
                TreeSet<Integer> rowSums = new TreeSet();
                rowSums.add(0);
                int comSum = 0;
                for (int row=0; row<matrix.length;row++){
                    int subSum = integralSums[row][j] - integralSums[row][i];
                    comSum += subSum;
                    Integer candidate = rowSums.ceiling(comSum-k);
                    candidate = candidate == null ? 0 : candidate;
                    if (comSum - candidate <= k){
                        closest = closest > comSum - candidate ? closest : comSum - candidate;
                    }
                    rowSums.add(comSum);
                }

            }
        }

        return closest;
    }
}
"""