class Solution:
    def maxChunksToSorted(self, arr: List[int]) -> int:
        min_end = [arr[-1]]
        for item in arr[-1::-1]:
            min_end.append(min(min_end[-1], item))

        min_end = min_end[::-1]

        chunks = 0

        i = 0
        while i < len(arr):
            j = i
            curr_max = arr[j]
            j += 1

            while j < len(arr):
                if min_end[j] < curr_max:
                    curr_max = max(curr_max, arr[j])
                    j += 1
                else:
                    break

            chunks += 1
            i = j

        return chunks
