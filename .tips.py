# sort tuples
elements = list(zip([5, 2, 3], [4, 9, 6]))
elements.sort(key=lambda x: x[0])
print(elements)

###############################################
# work with heap
from heapq import heappop, heappush, heapify

max_heap = []
heapify(max_heap)

heappush(max_heap, (-2, 'other info 1'))
heappush(max_heap, (-4, 'other info 2'))
heappush(max_heap, (-9, 'other info 3'))
max_value, _ = max_heap[0]  # get top value bit not extract
max_top, _ = heappop(max_heap)  # can't apply '-' operator to tuple
max_value = -max_value

###############################################
# deque
from collections import deque

sequence = deque()
sequence.append(2)
sequence.append(4)
sequence.append(3)
first = sequence.popleft()
last = sequence.pop()
last_exam = sequence[-1]    # not extract

# max int
import sys

m = sys.maxsize
# mod = int(1e9 + 7)
MOD = 1_000_000_007


###############################################
# DUS
class DSU:
    def find(self, item, parents):
        if parents[item] == item: return item
        parent = self.find(parents[item], parents)
        parents[item] = parent
        return parent

    def unite(self, p1, p2, parents):
        p1 = self.find(p1, parents)
        p2 = self.find(p2, parents)
        if p1 == p2: return
        parents[p2] = p1


###############################################
# trie
from collections import defaultdict


class Node:
    def __init__(self):
        self.neighbours = defaultdict(Node)
        self.is_terminate = False


def add_all(words):
    root = Node()
    for w in words:
        if len(w) > 0:
            add_word(root, w)


def add_word(root, word):
    node = root
    for w in word:
        node = node.neighbours[w]

    node.is_terminate = True


def search(root, word):
    node = root
    for w in word:
        if w in node.neighbours:
            node = node.neighbours[w]
        else:
            return False

    return node.is_terminate


###############################################
# greatest common divider
def gcd(self, a, b):
    if a == 0 or b == 0: return max(a, b)
    if a > b:
        return self.gcd(a % b, b)
    else:
        return self.gcd(a, b % a)


###############################################
# min array ends
arr = [2, 4, 5]
min_end = [arr[-1]]
for item in arr[-1::-1]:
    min_end.append(min(min_end[-1], item))

min_end = min_end[::-1]


###############################################
# bits manipulation
bit_mask = 0
bits = (1, 2, 5)
for bit in bits:
    bit_mask += (1 << bit)

# bm or and
bm_or = bit_mask | bit_mask
bm_and = bit_mask & bit_mask


###############################################
# binary search
def bin_search(array, value, begin, end):
    """
    Find smallest ind in array with array[ind] > value.
    If all items less => end + 1.
    """
    if len(array) == 0: return -1
    if array[begin] > value: return begin
    if end - begin <= 1:
        return end if array[end] > value else end + 1

    middle = (begin + end) // 2
    if array[middle] <= value:
        return bin_search(array, value, middle, end)
    else:
        return bin_search(array, value, begin, middle-1)
