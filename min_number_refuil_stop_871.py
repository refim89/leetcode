class Solution:
    def minRefuelStops(self, target, startFuel, stations):
        """
        :type target: int
        :type startFuel: int
        :type stations: List[List[int]]
        :rtype: int
        """
        max_stops = len(stations) + 1
        # i - stops made, j - current stop, max_fuel[i][j] - max fuel on stop j if i stops made before
        max_fuel = []
        for i in range(max_stops):
            max_fuel.append([-1]*max_stops)
        max_fuel[0][0] = startFuel

        for current_stop in range(1, max_stops):
            distance, fuel = stations[current_stop-1][0], stations[current_stop-1][1]
            for i in range(0, current_stop):
                if current_stop > 0 and max_fuel[i][current_stop-1] < distance: continue
                max_fuel[i][current_stop] = max(max_fuel[i][current_stop], max_fuel[i][current_stop-1])
                max_fuel[i + 1][current_stop] = max(max_fuel[i + 1][current_stop], max_fuel[i][current_stop-1] + fuel)

        # import numpy as np
        # print(np.array(max_fuel))

        for i in range(max_stops):
            if max_fuel[i][max_stops-1] >= target: return i
        return -1


if __name__ == '__main__':
    s = Solution()
    count = s.minRefuelStops(1, 1, stations=[])
    print(count)
    print()

    count = s.minRefuelStops(100, 1, stations=[[10, 100]])
    print(count)
    print()

    count = s.minRefuelStops(100, 10, stations=[[10,60],[20,30],[30,30],[60,40]])
    print(count)
    print()
