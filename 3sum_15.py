
# https://leetcode.com/problems/3sum/
# O(n^2) solution


class Solution:
    def threeSum(self, nums: 'List[int]') -> 'List[List[int]]':
        sequences = set()
        nums.sort()

        set_nums = set(nums)

        for i in range(0, len(nums)-2):
            for j in range(i+1, len(nums)-1):
                if nums[i] + nums[j] > 0: break
                additional = -(nums[i] + nums[j])

                if additional < nums[j]: break
                if additional not in set_nums: continue

                if additional == nums[j]:
                    if nums[j+1] == additional:
                        sequences.add((nums[i], nums[j], nums[j+1]))
                else:
                    sequences.add((nums[i], nums[j], additional))
        sequences = list(sequences)
        return sequences
