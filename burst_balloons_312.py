class Solution:

    """
    Task link:
    https://leetcode.com/problems/burst-balloons/description/
    """

    def maxCoins(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        n = len(nums)
        if n == 0: return 0

        # dp[i][j] - max sum on interval [i-j] (both i and j are included)
        # nums[i-1] and nums[j+1] are alive
        dp = [[0] * n for _ in range(n)]

        # iterate over interval distance
        for distance in range(0, n):
            for i in range(0, n - distance):
                j = i + distance
                previous = 1 if i == 0 else nums[i - 1]
                next = 1 if j == n - 1 else nums[j + 1]
                if i == j:
                    dp[i][j] = previous * nums[i] * next
                    continue

                max_sum = 0
                # iterate for last burst balloon if all in interval [i, (k-1)] and [(k+1), j] already burst
                for k in range(i, j + 1):
                    local_sum = previous * nums[k] * next
                    if k > i: local_sum += dp[i][k - 1]
                    if k < j: local_sum += dp[k+1][j]
                    max_sum = max(max_sum, local_sum)

                dp[i][j] = max_sum

        return dp[0][n-1]
        # import numpy as np
        # print(np.array(dp))


s = Solution()
print(s.maxCoins([3, 1, 5, 8]))
