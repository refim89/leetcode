# Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*' where:
#
# '.' Matches any single character.​​​​
# '*' Matches zero or more of the preceding element.

class Solution:
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """

        # dp[i][j] - True if text[i:] is matched by pattern[j:]
        dp = [[False] * (len(p) + 1) for _ in range(len(s) + 1)]
        # empty line is matched by empty pattern
        dp[-1][-1] = True

        for i in range(len(s), -1, -1):
            for j in range(len(p)-1, -1, -1):
                # i < len(s) used to cover case when pattern ands with useless *
                match_letter = i < len(s) and (p[j] == s[i] or p[j] == '.')
                if j + 1 < len(p) and p[j + 1] == '*':
                    # if we skip a* dp[i][j] = dp[i][j+2]
                    # else d[i][j] = match_letter and next letter was matched too
                    dp[i][j] = (match_letter and dp[i + 1][j]) or dp[i][j + 2]
                else:
                    dp[i][j] = match_letter and dp[i + 1][j + 1]

        return dp[0][0]


if __name__ == '__main__':
    s = Solution()
    c = s.isMatch("aaasc", ".*")
    print(c)
