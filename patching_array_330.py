class Solution:
    """
    Task link
    https://leetcode.com/problems/patching-array/description/

    Idea: lets `miss` - first missed value. If all values [0, miss) could be formed
    and nums[i]<=miss then all values up to miss+nums[i] could be also formed.
    if nums[i]>=miss then we have to insert miss and try again
    """

    def minPatches(self, nums, n):
        """
        :type nums: List[int]
        :type n: int
        :rtype: int
        """

        insert = 0
        miss = 1
        index = 0

        while miss <= n:
            if index< len(nums) and nums[index] <= miss:
                miss += nums[index]
                index += 1
            else:
                miss += miss
                insert += 1

        return insert


