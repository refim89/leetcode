# https://leetcode.com/problems/three-equal-parts/
# Idea: number of 1 in each ot three parts must be equal.  Find first and last 1 in each part and check if this
# sequences are the same. Count zeros after last part. Check if there ezists at least such number of zeros after
# first and second group. Special case: all zeros.

class Solution:
    def threeEqualParts(self, A: 'List[int]') -> 'List[int]':
        ones_count = sum(A)
        if ones_count == 0:
            return [0, 2]

        if ones_count % 3 != 0: return [-1, -1]

        expected_ones = ones_count // 3

        interval_begins = []
        interval_ends = []
        ones = 0
        for index in range(len(A)):
            if A[index] != 1: continue
            ones += 1
            if ones % expected_ones == 1 or expected_ones == 1:
                interval_begins.append(index)
            if ones % expected_ones == 0:
                interval_ends.append(index)

        interval_lengths = []
        for e, b in zip(interval_ends, interval_begins):
            interval_lengths.append(e - b + 1)

        if interval_lengths[0] != interval_lengths[1] or interval_lengths[1] != interval_lengths[2]:
            return [-1, -1]

        length = interval_lengths[0]

        i1, i2, i3 = interval_begins
        for i in range(length):
            if A[i + i1] != A[i + i2] or A[i + i2] != A[i + i3]:
                return [-1, -1]

        zeros_after1 = interval_begins[1] - interval_ends[0] - 1
        zeros_after2 = interval_begins[2] - interval_ends[1] - 1
        zeros_after3 = len(A) - 1 - interval_ends[2]

        if zeros_after1 < zeros_after3 or zeros_after2 < zeros_after3:
            return [-1, -1]

        return [interval_ends[0] + zeros_after3, interval_ends[1] + zeros_after3 + 1]


if __name__ == '__main__':
    s = Solution()
    c = s.threeEqualParts([1, 0, 1, 0, 1])
    print(c)
