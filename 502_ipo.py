from heapq import heappop, heappush, heapify


class Solution:

    def _extract(self, heap, expected_min, max_tasks):
        t = 0
        extracted = 0
        for _ in range(max_tasks):
            if len(heap) == 0:
                return extracted, t
            v = heappop(heap)
            extracted += -v
            t += 1
            if extracted >= expected_min:
                return extracted, t
        return extracted, t

    def findMaximizedCapital(self, k: int, W: int, Profits, Capital) -> int:
        items = len(Profits)
        if items == 0: return W

        capital_profit = list(zip(Capital, Profits))
        capital_profit.sort(key=lambda x: x[0])

        heap = []
        heapify(heap)

        current_capital = W
        tasks = 0

        for item in capital_profit:
            c, p = item

            if c <= current_capital:
                heappush(heap, -1 * p)
            else:
                profit_diff, tasks_diff = self._extract(heap, c - current_capital, k - tasks)
                current_capital += profit_diff
                tasks += tasks_diff
                if current_capital < c or tasks == k: return current_capital
                heappush(heap, -1 * p)

        profit_diff, tasks_diff = self._extract(heap, 1 << 32, k - tasks)
        current_capital += profit_diff

        return current_capital


if __name__ == '__main__':
    s = Solution()
    p = s.findMaximizedCapital(2, 0, [1,2,3], [0,1,1])
    print(p)
