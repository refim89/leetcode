
class Solution:

    """
    Task link
    https://leetcode.com/problems/longest-valid-parentheses/
    """

    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """

        # longest valid if last ")" in position i
        dp = [0] * len(s)

        maxlen = 0

        for i in range(1, len(s)):
            if s[i] == ')':
                # if sequence  like ...() just add 2 to dp[i-2]
                if s[i - 1] == '(':
                    dp[i] = 2 if i == 1 else dp[i - 2] + 2

                # if sequence  like ...))
                # check if before previous longest sequence {i-dp[i-1]-1} position was "("
                if s[i - 1] == ')':
                    if i - dp[i - 1] > 0 and s[i - dp[i - 1] - 1] == '(':
                        # dp[i - dp[i - 1] - 2] - valid seq before our "("
                        # + len of last valid + 2
                        dp[i] = dp[i - 1] + (dp[i - dp[i - 1] - 2] if (i - dp[i - 1]) >= 2 else 0) + 2

                maxlen = max(maxlen, dp[i])

        return maxlen

s = Solution()

# t = s.longestValidParentheses("))()()(()()))")
# t = s.longestValidParentheses("(()")
# t = s.longestValidParentheses("()(())")
t = s.longestValidParentheses("(()))())(")
print(t)
