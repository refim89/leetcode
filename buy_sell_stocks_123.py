class Solution:
    def maxProfit(self, prices) -> int:
        n = len(prices)
        if n <= 1: return 0
        begin_mins = [prices[0]]
        end_maxes = [0] * n
        end_maxes[-1] = prices[-1]

        for p in prices[1:]:
            begin_mins.append(min(begin_mins[-1], p))

        for i in range(n - 2, -1, -1):
            p = prices[i]
            end_maxes[i] = max(end_maxes[i + 1], p)

        # max profit for first transaction if we sell at day i
        from heapq import heappop, heappush, heapify

        max_heap = []
        heapify(max_heap)

        max_profit = 0
        for i in range(1, n):
            m = prices[i] - begin_mins[i - 1]
            if m > 0:
                heappush(max_heap, (-m, i))
                max_profit = max(max_profit, m)

        # max profit of both transactions
        # if second transaction starts at position i
        for i in range(n - 2, -1, -1):
            m = end_maxes[i + 1] - prices[i]
            if m > 0:
                # remove from heap items with large end
                while len(max_heap) > 0 and max_heap[0][1] >= i:
                    heappop(max_heap)

                if len(max_heap) > 0:
                    first_transaction_max, _ = max_heap[0]
                    first_transaction_max = -first_transaction_max
                    max_profit = max(max_profit, m + first_transaction_max)

        return max_profit
