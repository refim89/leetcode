from collections import defaultdict


class Solution:

    def min_cost(self, s, i, j, dp):
        if i >= j: return 0
        if (i, j) in dp: return dp[(i, j)]
        if s[i] == s[j]:
            dp[(i, j)] = self.min_cost(s, i + 1, j - 1, dp)
        else:
            dp[(i, j)] = 1 + min(self.min_cost(s, i+1, j, dp), self.min_cost(s, i, j-1, dp))

        return dp[(i, j)]

    def minInsertions(self, s: str) -> int:
        letters = len(s)
        dp = defaultdict(int)
        mincost = self.min_cost(s, 0, letters - 1, dp)
        return mincost


if __name__ == '__main__':
    s = Solution()
    t = s.minInsertions("mbadm")
    print(t)
